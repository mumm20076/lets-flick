//
//  PhotoGridCellViewModel.swift
//  ApolloMunich
//
//  Created by office on 03/02/19.
//  Copyright © 2019 Praveen Sharma. All rights reserved.
//

/*
 PhotoGridCellViewModel is an architectural component and responsible for the any business or view logic need for gridcells, to be performed before that can the present on the view layer.
 */

import UIKit

class PhotoGridCellViewModel: NSObject {

    var imgUrl : URL!
    var authorName: String
    var published: String
    
    override init() {
        imgUrl = URL(string: Constant.Strings.empty)
        authorName = Constant.Strings.empty
        published = Constant.Strings.empty
    }
    
}
