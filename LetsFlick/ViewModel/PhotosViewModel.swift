//
//  PhotosViewModel.swift
//  ApolloMunich
//
//  Created by office on 03/02/19.
//  Copyright © 2019 Praveen Sharma. All rights reserved.
//

/*
 PhotoGridCellViewModel is an architectural component and responsible for the any business or view logic and binding need for photoviewcontroller, to be performed before that can the present on the view layer.
 */

import UIKit

class PhotosViewModel: NSObject {
    
    typealias VoidClosureType = ()->Void

    var reloadCollectionView: VoidClosureType?
    let networkManager = NetworkManager()
    
    override init() {
        super.init()
        initFetch()
    }
    
    //MARK: This method fecthes and parse the response data, using network manager
    func initFetch() {
        networkManager.getFlickPhotos { (responseData, success) in
            self.processFlickResponse(response: responseData)
        }
    }
    
    private var gridCellViewModels: [PhotoGridCellViewModel] = [PhotoGridCellViewModel]() {
        didSet {
            self.reloadCollectionView?()
        }
    }
    
    var numberOfCells: Int {
        return gridCellViewModels.count
    }
    
    func getGridCellViewModel( at indexPath: IndexPath ) -> PhotoGridCellViewModel{
        return gridCellViewModels[indexPath.row]
    }
    
    // MARK: This method will process the parsed response and fill the also images data into the gridcellviewmodels
    private func processFlickResponse( response: ResponseModel ) {
        var cellViewModels = [PhotoGridCellViewModel]()
        for entry in response.entry{
            cellViewModels.append(createPhotoGridCellViewModel(entry: entry))
        }
        self.gridCellViewModels = cellViewModels
    }
    
    // MARK: This method will intitale a particular grid cell model with the individual parsed data from the responsemodel
    private func createPhotoGridCellViewModel(entry: ResponseModel) -> PhotoGridCellViewModel{
        let gridCellViewModel = PhotoGridCellViewModel()
        gridCellViewModel.imgUrl = URL(string: entry.link[1].href as String)!
        gridCellViewModel.authorName = entry.author.name as String
        gridCellViewModel.published = entry.published as String
        return gridCellViewModel
    }

}
