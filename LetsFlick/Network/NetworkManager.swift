//
//  NetworkManager.swift
//  ApolloMunich
//
//  Created by office on 03/02/19.
//  Copyright © 2019 Praveen Sharma. All rights reserved.
//

/*
    NetworkManager is responsible for the all the transactions with data over the network and finally shape them in the most usable form by the view or viewmodel or any other consumers.
 */

import UIKit
import Alamofire
import XMLMapper

class NetworkManager: NSObject {

    // MARK: This method will request the flickr API to get the response and parse it into the ResposeModel.
    func getFlickPhotos(completion: @escaping (_ result: ResponseModel, _ success: Bool)->())  {
        Alamofire.request(Constant.BaseUrls.url + Constant.EndPoints.photos, method: .get, parameters: nil).response { (response)->Void in
            print(response);
            let responseString = String(data: response.data!, encoding: String.Encoding.utf8) as String!
            let responseData = XMLMapper<ResponseModel>().map(XMLString: responseString!)
            completion(responseData!, true);
        }
    }
    
}
