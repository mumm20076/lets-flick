//
//  PhotoFullScreenViewController.swift
//  ApolloMunich
//
//  Created by office on 02/02/19.
//  Copyright © 2019 Praveen Sharma. All rights reserved.
//

/*
 PhotoFullScreenViewController is responsible for controlling and dislaying the UI components for displaying fullscreen images.
 */

import UIKit

class PhotoFullScreenViewController: UIViewController {

    @IBOutlet weak var imgFullScreen: UIImageView!
    
    var imgLink : URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgFullScreen.kf.indicatorType = .activity
        imgFullScreen.kf.setImage(with: imgLink)
        let shareBtn : UIBarButtonItem = UIBarButtonItem(title: Constant.Strings.share, style: UIBarButtonItem.Style.plain, target: self, action: #selector(btnTappedShare))
        self.navigationItem.rightBarButtonItem = shareBtn
    }

    // MARK: This method will share the image with various available resources and also can save the image to the
    //       iphone image gallery.
    @objc func btnTappedShare() {
        let imgText:String = "Flick Image"
        let objectsToShare:URL = imgLink
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,imgText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.mail]
        self.present(activityViewController, animated: true, completion: nil)
    }

}
