//
//  ViewController.swift
//  ApolloMunich
//
//  Created by office on 02/02/19.
//  Copyright © 2019 Praveen Sharma. All rights reserved.
//

/*
 PhotosViewController is responsible for controlling and dislaying the UI components for displaying list of all images requested from the flick
 API with metadata of images.
 */

import UIKit
import SwiftyXMLParser
import Alamofire
import XMLMapper
import Kingfisher

class PhotosViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var collectionView: UICollectionView!

    var responseData : ResponseModel?;
    let photoViewModel = PhotosViewModel()
    let resuseId = "PhotoGridCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        photoViewModel.reloadCollectionView = { [weak self] () in
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
    }
    
    // MARK: This is the collectionview method that return the number of cells in the collectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoViewModel.numberOfCells;
    }
    
    // MARK: This is the collectionview method that return the cells for collectionview
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: resuseId, for: indexPath) as! PhotoGridCell
        
        let cellViewModel = photoViewModel.getGridCellViewModel(at: indexPath)
        cell.imgView.kf.indicatorType = .activity
        cell.imgView.kf.setImage(with: cellViewModel.imgUrl)
        cell.lblName.text = cellViewModel.authorName
        cell.lblPublished.text = cellViewModel.published

        return cell
    }
    
    // MARK: This is the collectionview method that return the size of each cell for collectionview
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 2
        let hardCodedPadding:CGFloat = 8
        let itemHeight:CGFloat = 150
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        return CGSize(width: itemWidth, height: itemHeight)
    }

    // MARK: This is the collectionview method that handles the click of the each cell item in collectionview.
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constant.Storyboard.main, bundle:nil)
        let photoFullScreenViewController = storyBoard.instantiateViewController(withIdentifier: Constant.Storyboard.photoFullscreen) as! PhotoFullScreenViewController
        let cellViewModel = photoViewModel.getGridCellViewModel(at: indexPath)
        photoFullScreenViewController.imgLink = cellViewModel.imgUrl
        self.navigationController?.pushViewController(photoFullScreenViewController, animated: true)
    }

}

