//
//  PhotoGridCell.swift
//  ApolloMunich
//
//  Created by office on 02/02/19.
//  Copyright © 2019 Praveen Sharma. All rights reserved.
//

/*
 PhotoGridCell is responsible for rendering each item in the collectionview.
 */

import UIKit

class PhotoGridCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblPublished: UILabel!
    @IBOutlet weak var lblName: UILabel!
}
