//
//  ResponseModel.swift
//  ApolloMunich
//
//  Created by office on 02/02/19.
//  Copyright © 2019 Praveen Sharma. All rights reserved.
//


/*
  ResponseModel is an architecture component, responsible for holding the parsed information requested from the network
  layer of the architecture.
 */


import UIKit
import XMLMapper

class ResponseModel: XMLMappable {
    var nodeName: String!
    
    var entry: [ResponseModel]!
    var link: [ResponseModel]!
    var author: ResponseModel!
    var href: String!
    var name: String!
    var published: String!
    
    required init?(map: XMLMap) {
        entry = [ResponseModel]()
    }
    
    func mapping(map: XMLMap) {
        entry <- map["entry"]
        link <- map["link"]
        author <- map["author"]
        href <- map["_href"]
        name <- map["name"]
        published <- map["published"]
    }

}
