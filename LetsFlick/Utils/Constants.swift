//
//  Constants.swift
//  ApolloMunich
//
//  Created by office on 03/02/19.
//  Copyright © 2019 Praveen Sharma. All rights reserved.
//

/*
 Constant is the global constant file while includes all the necessary strings used in the whole project.
 */

import UIKit

struct Constant {
    
    struct BaseUrls {
        static var url = "https://api.flickr.com/"
    }
    
    struct EndPoints {
        static var photos = "services/feeds/photos_public.gne"
    }
    
    struct Strings {
        static var empty = ""
        static var share = "Share"
    }
    
    struct Storyboard {
        static var main = "Main"
        static var photoFullscreen = "PhotoFullScreenViewController"
    }
    
}
